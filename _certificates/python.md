---
layout: default
title: Introduction to Python
img_path: /static/images/py-intro.png
pdf: /static/certificates/py_certificate.pdf
---